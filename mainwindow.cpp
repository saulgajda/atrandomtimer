#include <QDateTime>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qsrand(QDateTime::currentDateTime().toMSecsSinceEpoch());
    min = 24 * 3600; // 1 day
    max = 4 * 7 * 24 * 3600; // 4 weeks
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    uint next_show_in_seconds = qrand() % ( max + 1 - min ) + min;
    QDateTime timestamp( QDateTime::currentDateTime().addSecs( next_show_in_seconds ) );
    ui->label->setText(timestamp.toString( "dddd, d MMM yyyy hh:mm:ss" ));
}
